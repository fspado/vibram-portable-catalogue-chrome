
var dbName = "Test2";
var dbVersion = '1.0';
var dbDescription = "Vibram Test";
var dbSize = 5 * 1024 * 1024;
var db;

function OpenDb() {
	if(debug) alert('OpenDb function called');
	if(!db) {
		if(debug) alert('Opening db');
		db = openDatabase(dbName,
						  dbVersion,
						  dbDescription,
						  dbSize
						 );
	} else {
		if(debug) alert('Db already opened');
	}
	if(debug) alert('OpenDb function ended');
}


function checkDbExistance(callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select existance');
								 tx.executeSql('SELECT version FROM existance', [], function(tx, results) { dbExistance(results, callback); }, function() { dbNotExists(callback); });
								},
				   errorDB);
}

function dbExistance(results, callback) {
	if(results.rows.length == 0) {
		dbNotExists();
	}
	else {
		if(debug) alert("Db already exists");
		// Get the user present in user table
		getUniqueUser(callback);
	}
}

function dbNotExists(callback) {
	OpenDb();
	db.transaction(generateDb, errorDB, callback);
}

function generateDb(tx) {
	if(debug) alert("generateDb function called");
	tx.executeSql('DROP TABLE IF EXISTS existance');
	tx.executeSql('CREATE TABLE IF NOT EXISTS existance (id unique, version)');
	tx.executeSql("INSERT INTO existance (id, version) VALUES (?, ?)", Array(0, dbVersion));
	tx.executeSql('DROP TABLE IF EXISTS users');
	tx.executeSql('CREATE TABLE IF NOT EXISTS users (id_utente, nome, cognome, email, password, hash, permessi, attivazione)');
	tx.executeSql('DROP TABLE IF EXISTS categorie');
	tx.executeSql('CREATE TABLE IF NOT EXISTS categorie (id_categorie, id_livello1, id_livello2, id_livello3, id_livello4, ordinamento_cat, codice_colore, status, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS categorie_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS categorie_contenuti (id_cat_contenuto, id_categoria, id_lingua, categoria_testo, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti (id_prodotto int, codice_prodotto, immagine, scala_misure, misure, misure_tecniche, dimensioni_foglio, allegato, numero_download, correlazione_automatica, nuovo, status, usa, usa_approvato, data_modifica, catalogo_exclusive)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_allegati');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_allegati (id_allegati, id_prodotto int, nome_allegato, tipo_allegato, estensione, numero_download, status, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_colori');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_colori (id_colori, codice_colore, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_colori_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_colori_contenuti (id_colori_contenuto, id_colori, id_lingua, testo_colori, data_modifica)');	
	tx.executeSql('DROP TABLE IF EXISTS prodotti_compound');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_compound (id_compound, ordinamento, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_compound_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_compound_contenuti (id_compound_contenuto, id_compound, id_lingua, testo_compound, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_construction');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_construction (id_construction, ordinamento, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_construction_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_construction_contenuti (id_construction_contenuto, id_construction, id_lingua, testo_construction, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_contenuti (id_contenuto, id_prodotto int, id_lingua, titolo, descrizione_repair, descrizione, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_country');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_country (id_country, ordinamento, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_country_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_country_contenuti (id_country_contenuto, id_country, id_lingua, testo_country, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_mescole');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_mescole (id_mescole, density, hardness, abrasion, elongation, stress, immagine , data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS prodotti_mescole_contenuti');
	tx.executeSql('CREATE TABLE IF NOT EXISTS prodotti_mescole_contenuti (id_mescole_contenuto, id_mescole, id_lingua, testo_mescole, descrizione_mescole, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_categorie');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_categorie (id_prodotto int, id_categoria, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_colori');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_colori (id_prodotto int, id_colori, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_compound');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_compound (id_prodotto int, id_compound, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_construction');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_construction (id_prodotto int, id_construction, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_country');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_country (id_prodotto int, id_country, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS relazione_prodotti_mescole');
	tx.executeSql('CREATE TABLE IF NOT EXISTS relazione_prodotti_mescole (id_prodotto int, id_mescole, data_modifica)');
	tx.executeSql('DROP TABLE IF EXISTS synchronization');
	tx.executeSql('CREATE TABLE IF NOT EXISTS synchronization (id int AUTO_INCREMENT, date, addedProducts, updatedProducts)');
	if(debug) alert("generateDb function ended");
}

function getUniqueUser(callback) {
	if(debug) alert("Function getUniqueUser called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select user');
								 tx.executeSql('SELECT * FROM users', [], function(tx, results) {returnUser(results, function(user) {checkUserOnlineStatus(user, callback);});}, errorDB);
								},
				   errorDB);
}

function checkUserOnlineStatus(user, callback) {
	if(!user) {
		callback(user);
	}
	else {
		var networkState = navigator.network.connection.type;
		if(debug2) alert("Connection type: " + networkState);
		if(networkState != Connection.NONE && networkState != Connection.UNKNOWN) {
			checkOnlineStatus(user.email, user.password, user, callback);
		}
		else {
			callback(user);
		}
	}
}

function checkOnlineStatus(username, password, user, callback) {
	$.ajax({
			url: 'http://www.vibram.com/appcheck/index.php?v=1&email=' + username + '&code=' + password,
			dataType: 'jsonp',
			jsonp: 'jsoncallback',
			timeout: 20000,
			success: function(data, status){
				if(debug) alert(data);
				//attivo: "si"
				//dati_utente: Object
					//attivazione: "15-02-2012 11:46:03"
					//cognome: "Mischiatti"
					//id_utente: "1"
					//nome: "Luca"
					//permessi: "open"
					//__proto__: Object
					//__proto__: Object
				if(data.attivo == "nonpresente") {
					callback();
					return;
				}
				if(data.attivo == "no") {
					callback();
					return;
				}
				if(data.attivo == "si") {
					callback(user);
					return;
				}
			},
			error: function(error){
				alert(error);
			}
		});
}

function checkAuthentication(username, password, returnAuthentication) {
	if(debug) alert("Function checkAuthentication called");
	OpenDb();
	// 1. controllo se online
	var networkState = navigator.network.connection.type;
	if(debug) alert("Connection type: " + networkState);
	//if(networkState != Connection.NONE && networkState != Connection.UNKNOWN) {
		// E' online -> controllo login su server remoto
		$.ajax({
			url: 'http://www.vibram.com/appcheck/index.php?v=1&email=' + username + '&code=' + password,
			dataType: 'jsonp',
			jsonp: 'jsoncallback',
			timeout: 20000,
			success: function(data, status){
				if(debug) alert(data);
				//attivo: "si"
				//dati_utente: Object
					//attivazione: "15-02-2012 11:46:03"
					//cognome: "Mischiatti"
					//id_utente: "1"
					//nome: "Luca"
					//permessi: "open"
					//__proto__: Object
					//__proto__: Object
				if(data.attivo == "nonpresente") {
					returnAuthentication(data.attivo);
					return;
				}
				if(data.attivo == "no") {
					returnAuthentication(data.attivo);
					return;
				}
				if(data.attivo == "si") {
					insertUserOnDb(data.dati_utente, username, password, function() { returnAuthentication(data.attivo); });
					return;
				}
			},
			error: function(error){
				alert(error);
			}
		});
	/*} else {
		if(debug) alert("local authentication");
		OpenDb();
		db.transaction(function(tx) {
									 if(debug) alert('select user');
									 tx.executeSql('SELECT * FROM users WHERE username = ? AND password = ?', [user.email], function(tx, results) {returnUser(user, results, callback);}, errorDB);
									},
					   errorDB);
	}*/
}

function insertUserOnDb(user, username, password, callback) {
	if(debug) alert("Function insertUserOnDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select user');
								 tx.executeSql('SELECT id_utente FROM users WHERE id_utente = ?', [user.id_utente], function(tx, results) {returnInsertUser(user, username, password, results, callback);}, errorDB);
								},
				   errorDB);
}

function returnInsertUser(user, username, password, results, callback) {
	if(debug) alert('User insert results:' + results.rows.length);
	if(results.rows.length == 0) {
		// l'utente non c'è e lo devo creare
		db.transaction(function(tx) {
								 if(debug) alert('insert user');
								 tx.executeSql("INSERT INTO users (id_utente, nome, cognome, email, password, permessi, attivazione) VALUES (?, ?, ?, ?, ?, ?, ?)", Array(user.id_utente, user.nome, user.cognome, username, password, user.permessi, user.attivazione), callback, errorDB);
								},
				   errorDB);
	}
	else {
		callback();
	}
}

function synchronizeDb(userId, callback) {
	if(debug2) alert("synchronizeDb function called");
	var networkState = navigator.network.connection.type;
	if(debug2) alert("Connection type: " + networkState);
	if(networkState != Connection.NONE && networkState != Connection.UNKNOWN) {
		// E' online -> controllo login su server remoto
		/*$.getJSON("http://www.vibram.com/appcheck/index.php?",
		  {
			v: 2,
			data: "2012-01-01",
			id: userId
		  },
		  function(data) {
			if(debug2) alert(data);
			OpenDb();
			db.transaction(function(tx) {populateDb(tx, data, callback);}, errorDB, function() {if(debug2) alert("Db populated succesfully");});
		  });*/
		  $.ajax({
			url: 'http://www.vibram.com/appcheck/index.php?v=2&data=2012-01-01&id=1',
			dataType: 'jsonp',
			jsonp: 'jsoncallback',
			timeout: 20000,
			success: function(data, status){
				if(debug2) alert(data);
				OpenDb();
				db.transaction(function(tx) {populateDb(tx, data, callback);}, errorDB, function() {if(debug2) alert("Db populated succesfully");});
			},
			error: function(error){
				alert(error);
			}
		});

	}
	else {
		callback("Connessione alla rete non disponibile");
	}
}

function populateDb(tx, data, callback) {
	var imagesToDownload = Array();
	var smallImagesToDownload = Array();
	var pdfToDownload = Array();
	for(tableIndex in data) {
		var table = data[tableIndex];
		//alert("tabella: " + table.nome_tabella);
		for(rowIndex in table.data) {
			var row = table.data[rowIndex];
			var properties = new Array();
			var parameters = new Array();
			var questionMarks = new Array();
			for(property in row) {
				properties.push(property);
				parameters.push(row[property]);
				questionMarks.push('?');
				if(table.nome_tabella == 'prodotti' && property == 'immagine') {
					var imgFileName = row[property];
					if(imgFileName.length > 0) {
						imagesToDownload.push(imgFileName);
						smallImagesToDownload.push(imgFileName);
					}
				}
				else if (table.nome_tabella == 'prodotti_allegati' && property == 'nome_allegato') {
					if(row['tipo_allegato'] == 'file') {
						pdfToDownload.push(row[property]);
					}
				}
				else if(table.nome_tabella == 'prodotti_contenuti' && property == 'descrizione_repair') {
					alert(row[property]);
				}
			}				
			var sqlString = 'INSERT INTO ' +table.nome_tabella + '(' + properties.join(',') + ') VALUES (' + questionMarks.join(',') + ')';
			tx.executeSql(sqlString, parameters);
		}
	}
	getFileSystem(imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
}

function getFileSystem(imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
																			downloadImage(fileSystem, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
																		}, fail);
}

var updatedImages = 0;
var addedImages = 0;
var remainingImages;
var remainingImagesSmall;
var remainingPdf;
var errorImages = 0;
var errorImagesSmall = 0;
var errorPdf = 0;

function downloadImage(fileSystem, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	//alert(fileSystem.name);
	//alert(imgFileName);
	addedImages = addedImages + imagesToDownload.length + smallImagesToDownload.length;
	remainingImages = imagesToDownload.length;
	remainingImagesSmall = smallImagesToDownload.length;
	remainingPdf = pdfToDownload.length;
	$("#download_ok").html(remainingImages--);
	$("#download_small_ok").html(remainingImagesSmall--);
	$("#download_pdf_ok").html(remainingPdf--);
	fileSystem.root.getDirectory("images", {create: true, exclusive: false},
							function(parent) {
										saveImage(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
							}, fail);
	
	fileSystem.root.getDirectory("images_small", {create: true, exclusive: false},
							function(parent) {
										saveImageSmall(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
							}, fail);
	fileSystem.root.getDirectory("pdf", {create: true, exclusive: false},
							function(parent) {
										savePdf(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
							}, fail);
}

function saveImage(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	//alert(parent.name);
	var fileTransfer = new FileTransfer();
	//for (imgIndex in imagesToDownload) {
	if(imagesToDownload.length > 0) {
		var imgFileName = imagesToDownload.pop();
		fileTransfer.download(
			"http://www.vibram.com/vibramrepair/immagini_caricate/" + imgFileName,
			parent.fullPath + "/" + imgFileName,
			function(entry) {
				//alert("download complete: " + entry.fullPath);
				downloadImageSuccess(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
			},
			function(error) {
				/*alert("download error source " + error.source);
				alert("download error target " + error.target);
				alert("upload error code" + error.code);*/
				downloadImageError(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
			}
		);
	}
	else if(smallImagesToDownload.length == 0 && pdfToDownload == 0) {
		insertLastSync(callback);
	}
}

function downloadImageSuccess(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	$("#download_ok").html(remainingImages--);
	saveImage(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
}

function downloadImageError(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	$("#download_error").html(++errorImages);
	saveImage(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
}

function saveImageSmall(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	//alert(parent.name);
	var fileTransfer = new FileTransfer();
	if(smallImagesToDownload.length > 0) {
		var imgFileName = smallImagesToDownload.pop();
		fileTransfer.download(
			"http://www.vibram.com/vibramrepair/immagini_caricate/piccole/" + imgFileName,
			parent.fullPath + "/" + imgFileName,
			function(entry) {
				//alert("download complete: " + entry.fullPath);
				downloadImageSmallSuccess(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
			},
			function(error) {
				/*alert("download error source " + error.source);
				alert("download error target " + error.target);
				alert("upload error code" + error.code);*/
				downloadImageSmallError(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
			}
		);
	}
	else if(imagesToDownload.length == 0 && pdfToDownload.length == 0) {
		insertLastSync(callback);
	}
}

function downloadImageSmallSuccess(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	$("#download_small_ok").html(remainingImagesSmall--);
	saveImageSmall(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
}

function downloadImageSmallError(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	$("#download_small_error").html(++errorImagesSmall);
	saveImageSmall(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
}

function savePdf(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	//alert(parent.name);
	var fileTransfer = new FileTransfer();
	if(pdfToDownload.length > 0) {
		var pdfFileName = pdfToDownload.pop();
		fileTransfer.download(
			"http://www.vibram.com/vibramrepair/immagini_caricate/piccole/" + pdfFileName,
			parent.fullPath + "/" + pdfFileName,
			function(entry) {
				//alert("download complete: " + entry.fullPath);
				downloadPdfSuccess(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
			},
			function(error) {
				/*alert("download error source " + error.source);
				alert("download error target " + error.target);
				alert("upload error code" + error.code);*/
				downloadPdfError(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
			}
		);
	}
	else if(imagesToDownload.length == 0 && smallImagesToDownload.length == 0) {
		insertLastSync(callback);
	}
}

function downloadPdfSuccess(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	$("#download_pdf_ok").html(remainingPdf--);
	savePdf(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
}

function downloadPdfError(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback) {
	$("#download_pdf_error").html(++errorPdf);
	savePdf(parent, imagesToDownload, smallImagesToDownload, pdfToDownload, callback);
}

function insertLastSync(callback) {
	db.transaction(function(tx) {
								 if(debug) alert('insert user');
								 tx.executeSql("INSERT INTO synchronization (date, addedProducts, updatedProducts) VALUES (date('now'), ?, ?)", Array(addedImages, updatedImages), callback, errorDB);
								},
				   errorDB);
}

function getUserFromDb(hash, callback) {
	if(debug) alert("Function getUser called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select user');
								 tx.executeSql('SELECT * FROM users WHERE hash = ?', [hash], function(tx, results) {returnUser(results, callback);}, errorDB);
								},
				   errorDB);
}

function returnUser(results, callback) {
	if(debug) alert('User results:' + results.rows.length);
	if(results.rows.length ==0) {
		callback();
	} else {
		var item = results.rows.item(0);
		var user = new User(item.id_utente, item.nome, item.cognome, item.email, item.password, item.hash, item.permessi, item.attivazione);
		callback(user);
	}
}

function getCategoriesFromDb(callback) {
	if(debug) alert("Function getCategoriesFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select categories');
								 tx.executeSql('SELECT id_categorie, id_livello1, id_livello2, id_livello3, id_livello4, ordinamento_cat, status, categoria_testo, id_lingua FROM categorie JOIN categorie_contenuti ON categorie.id_categorie = categorie_contenuti.id_categoria WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractCategories(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractCategories(results, callback) {
	if(debug) alert('Categories results:' + results.rows.length);
	var allCategories = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).categoria_testo);
		allCategories.push(new Category(results.rows.item(i).id_categorie, results.rows.item(i).id_livello1, results.rows.item(i).id_livello2, results.rows.item(i).id_livello3, results.rows.item(i).id_livello4, results.rows.item(i).ordinamento_cat, results.rows.item(i).categoria_testo));
	}
	callback(allCategories);
}

function getCompoundsFromDb(callback) {
	if(debug) alert("Function getCompoundFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select compounds');
								 tx.executeSql('SELECT prodotti_compound.id_compound, ordinamento, testo_compound FROM prodotti_compound JOIN prodotti_compound_contenuti ON prodotti_compound.id_compound = prodotti_compound_contenuti.id_compound WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractCompounds(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractCompounds(results, callback) {
	if(debug) alert('Compounds results:' + results.rows.length);
	var allCompounds = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).testo_compound);
		allCompounds.push(new Compound(results.rows.item(i).id_compound, results.rows.item(i).ordinamento, results.rows.item(i).testo_compound));
	}
	callback(allCompounds);
}

function getCountriesFromDb(callback) {
	if(debug) alert("Function getCountriesFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select countries');
								 tx.executeSql('SELECT prodotti_country.id_country, ordinamento, testo_country FROM prodotti_country JOIN prodotti_country_contenuti on prodotti_country.id_country = prodotti_country_contenuti.id_country WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractCountries(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractCountries(results, callback) {
	if(debug) alert('Countries results:' + results.rows.length);
	var allCountries = new Array();
	for(i=0; i<results.rows.length; i++) {
		if(debug) alert(results.rows.item(i).testo_country);
		allCountries.push(new Country(results.rows.item(i).id_country, results.rows.item(i).ordinamento, results.rows.item(i).testo_country));
	}
	callback(allCountries);
}


function getConstructionsFromDb(callback) {
	if(debug) alert("Function getConstructionsFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select constructions');
								 tx.executeSql('SELECT prodotti_construction.id_construction, ordinamento, testo_construction FROM prodotti_construction JOIN prodotti_construction_contenuti on prodotti_construction.id_construction = prodotti_construction_contenuti.id_construction WHERE id_lingua = ?', Array('2'), function(tx, results) { exctractConstructions(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractConstructions(results, callback) {
	if(debug) alert('Constructions results:' + results.rows.length);
	var allConstructions = new Array();
	for(i=0; i<results.rows.length; i++) {
		if(debug) alert(results.rows.item(i).testo_construction);
		allConstructions.push(new Construction(results.rows.item(i).id_construction, results.rows.item(i).ordinamento, results.rows.item(i).testo_construction));
	}
	callback(allConstructions);
}

function getProductListFromDb(filters, sortOrder, order, start, count, exclusive, searchString, callback) {
	if(debug) alert("Function getProductListFromDb called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select products');
								 var parameters = new Array();
								 var sqlQuery = "SELECT prodotti.id_prodotto, codice_prodotto, immagine, nuovo, titolo, relazione_prodotti_country.id_country FROM prodotti " + 
								 				"LEFT JOIN relazione_prodotti_categorie ON prodotti.id_prodotto = relazione_prodotti_categorie.id_prodotto " +
												"LEFT JOIN relazione_prodotti_construction ON prodotti.id_prodotto = relazione_prodotti_construction.id_prodotto " +
												"LEFT JOIN relazione_prodotti_compound ON prodotti.id_prodotto = relazione_prodotti_compound.id_prodotto " +
												"LEFT JOIN relazione_prodotti_country ON prodotti.id_prodotto = relazione_prodotti_country.id_prodotto " +
												"LEFT JOIN prodotti_contenuti ON prodotti.id_prodotto = prodotti_contenuti.id_prodotto " +
												"WHERE status = 'si' AND (usa = 'si' OR usa = 'no') AND (usa_approvato = 'si' OR usa_approvato = 'no') AND id_lingua = '2'";
								 for(filterIndex in filters) {
									var filter = filters[filterIndex];
									if(filter.filterValues.length > 0) {
										if(filter.filterName == 'category') {
											sqlQuery += " AND id_categoria in (";
											for(filterValueIndex in filter.filterValues) {
												var filterValue = filter.filterValues[filterValueIndex];
												sqlQuery += "'" + filterValue + "'";
											}
											sqlQuery += ")";
										}
										else if(filter.filterName == 'construction') {
											sqlQuery += " AND id_construction in (";
											for(filterValueIndex in filter.filterValues) {
												var filterValue = filter.filterValues[filterValueIndex];
												sqlQuery += "'" + filterValue + "'";
											}
											sqlQuery += ")";
										}
										else if(filter.filterName == 'compound') {
											sqlQuery += " AND id_compound in (";
											for(filterValueIndex in filter.filterValues) {
												var filterValue = filter.filterValues[filterValueIndex];
												sqlQuery += "'" + filterValue + "'";
											}
											sqlQuery += ")";
										}
										else if(filter.filterName == 'country') {
											sqlQuery += " AND id_country in (";
											for(filterValueIndex in filter.filterValues) {
												var filterValue = filter.filterValues[filterValueIndex];
												sqlQuery += "'" + filterValue + "'";
											}
											sqlQuery += ")";
										}
									}
								 }
								 if(searchString && searchString.length > 0) {
									 sqlQuery += " AND (codice_prodotto like ? OR titolo like ?)";
									 parameters.push(searchString + "%");
									 parameters.push(searchString + "%");
								 }
								 sqlQuery += ' GROUP BY prodotti.id_prodotto';
								 if(sortOrder == 'titolo') {
									 sqlQuery += ' ORDER BY prodotti_contenuti.' + sortOrder + ' ' + order;
								 } else {
									 sqlQuery += ' ORDER BY prodotti.' + sortOrder + ' ' + order;
								 }
								 tx.executeSql(sqlQuery, parameters, function(tx, results) { exctractProductLists(results, filters, start, count, callback); }, errorDB);
								},
				   errorDB);
}

function exctractProductLists(results, filters, start, count, callback) {
	if(debug) alert('Products results:' + results.rows.length);
	var allProducts = new Array();
	var limit = Math.min(start + count, results.rows.length);
	for(i=start; i<limit; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(results.rows.item(i).categoria_testo);
		allProducts.push(new Product(item.id_prodotto, item.codice_prodotto, item.titolo, null, null, item.id_country, null, item.nuovo, item.immagine, null, null, null, null, null, null, null, null));
	}
	callback(allProducts, results.rows.length);
}

//-------------------------------------------LUCA START
function getProductFromDb(idProduct, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select product full detail');
								 	 var sqlQuery = "SELECT * FROM prodotti " +
												"LEFT JOIN prodotti_contenuti ON prodotti.id_prodotto = prodotti_contenuti.id_prodotto " +
												"LEFT JOIN relazione_prodotti_country ON prodotti.id_prodotto = relazione_prodotti_country.id_prodotto " +
												"WHERE prodotti.id_prodotto=? AND status = 'si' AND (usa = 'si' OR usa = 'no') AND (usa_approvato = 'si' OR usa_approvato = 'no') AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(idProduct, '2'), function(tx, results) { exctractProductDetail(results, callback); }, errorDB);
								},
				   errorDB);
}

function exctractProductDetail(results, callback) {
	if(debug) alert('Product detail:' + results.rows.length);
	var allDetails = new Array();
	for(i=0; i<results.rows.length; i++) {
		if(debug) alert(results.rows.item(i).id_prodotto);
		allDetails.push(new Product(results.rows.item(i).id_prodotto, results.rows.item(i).codice_prodotto, results.rows.item(i).titolo, null, null, results.rows.item(i).id_country, null, results.rows.item(i).nuovo, results.rows.item(i).immagine, results.rows.item(i).immagine, results.rows.item(i).descrizione, results.rows.item(i).descrizione_repair, results.rows.item(i).scala_misure, results.rows.item(i).misure, results.rows.item(i).misure_tecniche, results.rows.item(i).dimensioni_foglio, results.rows.item(i).allegato));
	}
	getProductImageFromFileSystem(allDetails[0], callback)
}

function getProductImageFromFileSystem(product, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						resolveDirectoryImages(fileSystem, product, callback);
					}, fail);
}

function resolveDirectoryImages(fileSystem, product, callback) {
	fileSystem.root.getDirectory("images", {create: false, exclusive: false},
							function(parent) {
										if(product.imageBig.length > 0) {
											product.imageBig = parent.toURI() + "/" + product.imageBig;
										}
										else {
											product.imageBig = "";
										}
										//product.image = "/Documents/images_small/" + product.image;
										//alert(product.image);
										callback(product);
							}, fail);
}

function getColorsFromDb(product, callback) {
	if(debug) alert("Function getColors called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select colors');
								 tx.executeSql('SELECT * FROM prodotti_colori JOIN prodotti_colori_contenuti ON prodotti_colori.id_colori = prodotti_colori_contenuti.id_colori JOIN relazione_prodotti_colori ON prodotti_colori.id_colori=relazione_prodotti_colori.id_colori  WHERE id_lingua = ? AND  relazione_prodotti_colori.id_prodotto = ?', Array('2',product.id), function(tx, results) { exctractColors(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function exctractColors(results, product, callback) {
	if(debug) alert('Colors results:' + results.rows.length);
	var allColors = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		if(debug) alert(results.rows.item(i).codice_colore);
		allColors.push(new Color(results.rows.item(i).id_colori, results.rows.item(i).codice_colore, results.rows.item(i).testo_colori));
	}
	callback(allColors);
}

function getRelatedAttachmentsFromDb(product, callback) {
	if(debug) alert("Function getColors called");
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select attachment');
								 tx.executeSql('SELECT * FROM prodotti_allegati WHERE status = ? AND  id_prodotto = ?', Array('si',product.id), function(tx, results) { exctractRelatedAttachments(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function exctractRelatedAttachments(results, product, callback) {
	if(debug) alert('Attachments results:' + results.rows.length);
	var allAttachments = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		allAttachments.push(new Attachment(item.id_allegati, item.nome_allegato, item.tipo_allegato, item.estensione, null, null));
	}
	callback(allAttachments);
}

function getAttachmentImageDetailsFromDb(attachment, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						resolveDirectoryAttachmentSmallImage(fileSystem, attachment, callback);
					}, fail);
}

function resolveDirectoryAttachmentSmallImage(fileSystem, attachment, callback) {
	fileSystem.root.getDirectory("images_small", {create: false, exclusive: false},
							function(parent) {
										if(attachment.attachmentName.length > 0) {
											attachment.imageSmall = parent.toURI() + "/" + attachment.attachmentName;
										}
										else {
											attachment.imageSmall = "";
										}
										resolveDirectoryAttachmentBigImage(fileSystem, attachment, callback);
							}, fail);
}

function resolveDirectoryAttachmentBigImage(fileSystem, attachment, callback) {
	fileSystem.root.getDirectory("images", {create: false, exclusive: false},
							function(parent) {
										if(attachment.attachmentName.length > 0) {
											attachment.imageBig = parent.toURI() + "/" + attachment.attachmentName;
										}
										else {
											attachment.imageBig = "";
										}
										callback(attachment);
							}, fail);
}


//-------------------------------------------LUCA END

function getProductShortDetailFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select product short detail');
								 var sqlQuery = "SELECT categoria_testo FROM relazione_prodotti_categorie JOIN categorie_contenuti ON relazione_prodotti_categorie.id_categoria = categorie_contenuti.id_categoria WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultCategoryProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultCategoryProduct(results, product, callback) {
	if(debug) alert('Product categories results:' + results.rows.length);
	var allCategories = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.categoria_testo);
		allCategories.push(item.categoria_testo);
	}
	product.categories = allCategories;
	getProductCountriesFromDb(product, callback);
}

function getProductCountriesFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select countries short detail');
								 var sqlQuery = "SELECT testo_country FROM relazione_prodotti_country JOIN prodotti_country_contenuti ON relazione_prodotti_country.id_country = prodotti_country_contenuti.id_country WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultCountryProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultCountryProduct(results, product, callback) {
	if(debug) alert('Product countries results:' + results.rows.length);
	var allCountries = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.testo_country);
		allCountries.push(item.testo_country);
	}
	product.countries = allCountries;
	getProductCompoundsFromDb(product, callback);
}

function getProductCompoundsFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select compounds short detail');
								 var sqlQuery = "SELECT testo_compound FROM relazione_prodotti_compound JOIN prodotti_compound_contenuti ON relazione_prodotti_compound.id_compound = prodotti_compound_contenuti.id_compound WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultCompoundProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultCompoundProduct(results, product, callback) {
	if(debug) alert('Product compounds results:' + results.rows.length);
	var allCompounds = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.testo_compound);
		allCompounds.push(item.testo_compound);
	}
	product.compounds = allCompounds;
	getProductConstructionsFromDb(product, callback);
}

function getProductConstructionsFromDb(product, callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select constructions short detail');
								 var sqlQuery = "SELECT testo_construction FROM relazione_prodotti_construction JOIN prodotti_construction_contenuti ON relazione_prodotti_construction.id_construction = prodotti_construction_contenuti.id_construction WHERE id_prodotto = ? AND id_lingua = ?";
								 tx.executeSql(sqlQuery, Array(product.id, '2'), function(tx, results) { resultConstructionProduct(results, product, callback); }, errorDB);
								},
				   errorDB);
}

function resultConstructionProduct(results, product, callback) {
	if(debug) alert('Product contruction results:' + results.rows.length);
	var allConstructions = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i); 
		if(debug) alert(item.testo_construction);
		allConstructions.push(item.testo_construction);
	}
	product.constructions = allConstructions;
	getProductImageSmallFromFileSystem(product, callback);
}

function getProductImageSmallFromFileSystem(product, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						resolveDirectorySmallImages(fileSystem, product, callback);
					}, fail);
}

function resolveDirectorySmallImages(fileSystem, product, callback) {
	fileSystem.root.getDirectory("images_small", {create: false, exclusive: false},
							function(parent) {
										if(product.image.length > 0) {
											product.image = parent.toURI() + "/" + product.image;
										} else {
											product.image = "";
										}
										//product.image = "/Documents/images_small/" + product.image;
										//alert(product.image);
										callback(product);
							}, fail);
}

function getSyncFromDb(callback) {
	OpenDb();
	db.transaction(function(tx) {
								 if(debug) alert('select sync short detail');
								 var sqlQuery = "SELECT * FROM synchronization";
								 tx.executeSql(sqlQuery,[], function(tx, results) { resultSyncFromDb(results, callback); }, errorDB);
								},
				   errorDB);
}

function resultSyncFromDb(results, callback) {
//	synchronization (id int AUTO_INCREMENT, date, addedProducts, updatedProducts)
if(debug) alert('Sync results:' + results.rows.length);
	var allSync = new Array();
	for(i=0; i<results.rows.length; i++) {
		var item = results.rows.item(i);
		allSync.push(new Sync(item.id, item.date, item.addedProducts, item.updatedProducts));
	}
	callback(allSync);
}

// Transaction error callback
function errorDB(tx, err) {
	alert("tx error code: " + tx.code);
	alert("tx error message: " + tx.message);
	if(err) {
		alert("Error processing SQL: " + err.message);
	}
}

function fail(e) {
	var msg = '';

  switch (e.code) {
    case FileError.QUOTA_EXCEEDED_ERR:
      msg = 'QUOTA_EXCEEDED_ERR';
      break;
    case FileError.NOT_FOUND_ERR:
      msg = 'NOT_FOUND_ERR';
      break;
    case FileError.SECURITY_ERR:
      msg = 'SECURITY_ERR';
      break;
    case FileError.INVALID_MODIFICATION_ERR:
      msg = 'INVALID_MODIFICATION_ERR';
      break;
    case FileError.INVALID_STATE_ERR:
      msg = 'INVALID_STATE_ERR';
      break;
    default:
      msg = 'Unknown Error';
      break;
  };

  alert('Error: ' + msg);
}
