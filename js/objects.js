
var debug = false;
var debug2 = false;

window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
var LocalFileSystem = LocalFileSystem || window;

function User(id, firstname, lastname, email, password, hash, permission, activationState) {
	this.id = id;
	this.firstname = firstname;
	this.lastname = lastname;
	this.email = email;
	this.password = password;
	this.hash = hash;
	this.permission = permission;
	this.activationState = activationState;
}

function Category(id, level1Id, level2Id, level3Id, level4Id, order, text) {
	this.id = id;
	this.level1Id = level1Id;
	this.level2Id = level2Id;
	this.level3Id = level3Id;
	this.level4Id = level4Id;
	this.order = order;
	this.text = text;	
}

function Compound(id, order, text) {
	this.id = id;
	this.order = order;
	this.text = text;
}

function Country(id, order, text) {
	this.id = id;
	this.order = order;
	this.text = text;
}

function Construction(id, order, text) {
	this.id = id;
	this.order = order;
	this.text = text;
}

function Color(id, code, text) {
	this.id = id;
	this.code = code;
	this.text = text;
}

function Attachment(id, attachmentName, attachmentType, extension, imageSmall, imageBig) {
	this.id = id;
	this.attachmentName = attachmentName;
	this.attachmentType = attachmentType;
	this.extension = extension;
	this.imageSmall = imageSmall;
	this.imageBig = imageBig;
}

// PARAMETERS: filterName:enum{category, construction, country, compound} -> one of the available filters
//			   filterValues:string[] -> an array of filter ids
function Filter(filterName, filterValues) {
	this.filterName = filterName;
	this.filterValues = filterValues;
}

function Product(id, code, name, categories, constructions, countryId, compounds, isNew, image, imageBig, description, descriptionRepair, measureScale, measure, measureTech, dimension, attach, mescole) {
	this.id = id;
	this.code = code;
	this.name = name;
	this.categories = categories;
	this.constructions = constructions;
	this.countryId = countryId;
	this.compounds = compounds;
	this.isNew = isNew;
	this.image = image;
	this.imageBig = imageBig;
	this.description = description;
	this.descriptionRepair = descriptionRepair;
	this.measureScale = measureScale;
	this.measure = measure;
	this.measureTech = measureTech;
	this.dimension = dimension;
	this.attach = attach;
	this.mescole = mescole;
}

function Sync(id, date, addedProducts, updatedProducts, maxDate) {
	this.id = id;
	this.date = date;
	this.addedProducts = addedProducts;
	this.updatedProducts = updatedProducts;
	this.maxDate = maxDate;
}
function Brand(id, name, website) {
	this.id = id;
	this.name = name;
	this.website = website;
}

function DbPatch(fromVersion,toVersion,updateString)
{
	this.updateString=updateString;
	this.fromVersion=fromVersion;
	this.toVersion=toVersion;
}